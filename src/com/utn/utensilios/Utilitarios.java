package com.utn.utensilios;

import java.util.Scanner;

public class Utilitarios {

    public static double capturarDouble(String Peticion){
        double valor= 0;
        boolean continuar=true;
        do {
            try {
                System.out.println(Peticion);
                Scanner teclado=new Scanner(System.in);
                valor= teclado.nextDouble();
                continuar=false;
            }catch (Exception e){
                System.out.println("Error caracter invalido");
            }
        }while(continuar);
        return valor;
    }

    public static int capturarInt(String Peticion){
        int valor= 0;
        boolean continuar=true;
        do {
            try {
                System.out.print(Peticion);
                Scanner teclado=new Scanner(System.in);
                valor= teclado.nextInt();
                continuar=false;
            }catch (Exception e){
                System.out.println("Error caracter invalido");
            }
        }while(continuar);
        return valor;
    }

    public static String capturarString(String Peticion){
        String valor="";
        System.out.print(Peticion);
        Scanner teclado=new Scanner(System.in);
        valor=teclado.nextLine();
        return valor;
    }
}
