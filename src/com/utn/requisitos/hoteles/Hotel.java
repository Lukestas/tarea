package com.utn.requisitos.hoteles;

public class Hotel {
    private String texto1;
    private int cantidad1;
    private String texto2;
    private int cantidad2;

    public Hotel(String ptexto1, int pcantidad1,String ptexto2, int pcantidad2) {
        this.texto1 = ptexto1;
        this.cantidad1 = pcantidad1;
        this.texto2 = ptexto2;
        this.cantidad2 = pcantidad2;
    }

    public String almacen(){
        String retorno="";
        retorno+="Producto 1: "+this.texto1+", Cantidad: "+this.cantidad1+"\n" +
                "Producto 2: "+this.texto2+", Cantidad: "+this.cantidad2;
        return retorno;
    }

    public String getTexto1() {
        return texto1;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public int getCantidad1() {
        return cantidad1;
    }

    public void setCantidad1(int cantidad1) {
        this.cantidad1 = cantidad1;
    }

    public String getTexto2() {
        return texto2;
    }

    public void setTexto2(String texto2) {
        this.texto2 = texto2;
    }

    public int getCantidad2() {
        return cantidad2;
    }

    public void setCantidad2(int cantidad2) {
        this.cantidad2 = cantidad2;
    }
}