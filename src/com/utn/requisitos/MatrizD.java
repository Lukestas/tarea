package com.utn.requisitos;

public class MatrizD {
    private int[][] Matriz;

    public MatrizD(int filas, int columnas){
        this.Matriz=new int[filas][columnas];
    }

    private void inicializar(){
        for(int f=0;f<this.Matriz.length;f++){
            for(int c=0;c<this.Matriz[f].length;c++){
                this.Matriz[f][c]=(int)(Math.random()*20);
            }
        }
    }

    public void randomizar(){
        inicializar();
    }

    public String estado(){
        String retorno="";
        int cantidad=0;
        for(int f=0;f<this.Matriz.length;f++) {
            for (int c = 0; c < this.Matriz[c].length; c++) {
                if (this.Matriz[f][c]==5){
                    cantidad++;
                }
                if (c==2) {
                    retorno += this.Matriz[f][c];
                }
                else{
                    retorno += this.Matriz[f][c] + ",";
                }
            }
            retorno+="\n";

        }
        return retorno+"\nCantidad de numeros 5: "+cantidad;
    }

    public int[][] getMatriz() {
        return Matriz;
    }

    public void setMatriz(int[][] matriz) {
        Matriz = matriz;
    }
}
