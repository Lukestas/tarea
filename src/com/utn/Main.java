package com.utn;

import com.utn.requisitos.hoteles.Hotel;
import com.utn.requisitos.MatrizD;
import com.utn.utensilios.Utilitarios;

public class Main {

    public static void main(String[] args) {
    	System.out.println("---------Matriz---------");
		MatrizD matriz= new MatrizD(4,3);
		System.out.println(matriz.estado());
		System.out.println("----------------------------");
		System.out.println("Llenando Matriz");
		System.out.println("----------------------------");
		matriz.randomizar();
		System.out.println(matriz.estado());
		System.out.println("----------------------------\n\n\n");

		System.out.println("Objetos de hotel2");
		String objeto1= Utilitarios.capturarString("Nombre del producto: ");
		int cantidad1= Utilitarios.capturarInt("Cantidad de productos: ");
		String objeto2= Utilitarios.capturarString("Nombre del producto: ");
		int cantidad2= Utilitarios.capturarInt("Cantidad de productos: ");
		Hotel hotelito= new Hotel(objeto1,cantidad1,objeto2,cantidad2);
		System.out.println(hotelito.almacen());
		System.out.println(hotelito.getTexto1()+" "+hotelito.getCantidad1());
		System.out.println(hotelito.getTexto2()+" "+hotelito.getCantidad2());
    }
}
